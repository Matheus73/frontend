import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './quasar'
import apolloProvider from "./services/apolloClient";

Vue.config.productionTip = false

new Vue({
  router,
  apolloProvider,
  store,
  render: h => h(App)
}).$mount('#app')
