import Vue from 'vue';
import ApolloClient from 'apollo-boost';
import VueApollo from 'vue-apollo';

Vue.use(VueApollo);

const API_KEY = process.env.VUE_APP_APOLLO_SWOP_API_KEY;

export default new VueApollo({
	defaultClient: new ApolloClient({
		uri: 'https://swop.cx/graphql',
		headers: {
			Authorization: `ApiKey ${API_KEY}`,
		},
	}),
});
