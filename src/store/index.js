import ApolloClient from '../services/apolloClient';
import gql from 'graphql-tag';
import Vue from 'vue';
import Vuex from 'vuex';
import * as getters from './getters';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		currencyType: 'USD',
		invoiceValue: 0,
		tipPercent: 10,
		numberPeople: 2,
		totalInReal: 0,
		isLoading: false,
	},
	getters,
	mutations: {
		setCurrencyType(state, type) {
			state.currencyType = type;
		},
		setInvoiceValue(state, value) {
			state.invoiceValue = value;
		},
		setTipPercent(state, tip) {
			state.tipPercent = tip;
		},
		setNumberPeople(state, peoples) {
			state.numberPeople = peoples;
		},
		setTotalInReal(state, value) {
			state.totalInReal = value;
		},
		setIsLoading(state, value) {
			state.isLoading = value;
		},
		setClearFormValues(state) {
			state.currencyType = 'USD';
			state.invoiceValue = 0;
			state.tipPercent = 10;
			state.numberPeople = 2;
			state.totalInReal = 0;
			state.isLoading = false;
		},
	},
	actions: {
		async convertToBRL({ state, commit, getters }) {
			try {
				commit('setIsLoading', true);
				if (getters.tipTotal === 0) {
					commit('setTotalInReal', 0);
				} else {
					const query = gql`
				query ConvertToGBP {
					convert(
						amount: ${getters.tipTotal}
						baseCurrency: "${state.currencyType}"
						quoteCurrencies: ["BRL"]
					) {
						quoteAmount
					}
				}
			`;
					const res = await ApolloClient.defaultClient.query({
						query,
					});

					const quoteAmount = res.data.convert[0].quoteAmount;
					commit('setTotalInReal', quoteAmount);
				}
			} catch (error) {
				console.error(error);
				commit('setClearFormValues');
				throw new Error(error);
			} finally {
				commit('setIsLoading', false);
			}
		},
	},
});
