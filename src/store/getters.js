const currencySymbol = (state) => {
	if (state.currencyType === 'EUR') {
		return '€';
	}
	return '$';
};

const tipValue = (state) => {
	return state.invoiceValue * (state.tipPercent / 100);
};

const tipTotal = (state, getters) => {
	const tipValue = Number(getters.tipValue) || 0;
	const invoiceValue = Number(state.invoiceValue) || 0;
	return tipValue + invoiceValue;
};

const totalByPeople = (state, getters) => {
	const tipTotal = Number(getters.tipTotal) || 0;
	const numberPeople = Number(state.numberPeople) || 2;

	return tipTotal / numberPeople;
};

export { currencySymbol, tipValue, tipTotal, totalByPeople };
